/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * Alumno DAVID, Pedro
  * Legajo: 159.032-7
  * Alternativa 2
  * Aclaraciones: El código mio comienza en la linea 213. Tuve que modificar
  * el archivo FreeRTOSConfig.h y aumentar el tamaño del HEAP por 4096.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
void TareaLed1(void *p);
void TareaLed2(void *p);
void TareaLed3(void *p);
void TareaPrior(void *p);
uint8_t antirebote(void);
unsigned long rand(void);

  xTaskHandle pTareaLED1;
  xTaskHandle pTareaLED2;
  xTaskHandle pTareaLED3;

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */


  xTaskCreate(TareaLed1, "TareaLed1" , 128 , NULL , 1, &pTareaLED1);
  xTaskCreate(TareaLed2, "TareaLed2" , 128 , NULL , 1 , &pTareaLED2);
  xTaskCreate(TareaLed3, "TareaLed3" , 128 , NULL , 1 , &pTareaLED3);
  xTaskCreate(TareaPrior, "TareaPrior" , 128 , NULL , 3, NULL);
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
 
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, Led3_Pin|Led2_Pin|Led1_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : Led3_Pin Led2_Pin Led1_Pin */
  GPIO_InitStruct.Pin = Led3_Pin|Led2_Pin|Led1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : Boton_Pin */
  GPIO_InitStruct.Pin = Boton_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(Boton_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void TareaLed1(void *p){

	while(1){
		HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, 1);
		HAL_GPIO_WritePin(Led2_GPIO_Port, Led2_Pin, 0);
		HAL_GPIO_WritePin(Led3_GPIO_Port, Led3_Pin, 0);
	}

}
void TareaLed2(void *p){

	while(1){
		HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, 0);
		HAL_GPIO_WritePin(Led2_GPIO_Port, Led2_Pin, 1);
		HAL_GPIO_WritePin(Led3_GPIO_Port, Led3_Pin, 0);
	}

}
void TareaLed3(void *p){

	while(1){
		HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, 0);
		HAL_GPIO_WritePin(Led2_GPIO_Port, Led2_Pin, 0);
		HAL_GPIO_WritePin(Led3_GPIO_Port, Led3_Pin, 1);
	}

}
void TareaPrior(void *p){ //Esta tarea debe hacer el antirebote y el cambio de prioridad

	uint8_t estado = ESPERA;
	uint8_t aux = 0;  // 0 = LED1, 1 = LED2, 2 = LED3, 3 = RAND
	unsigned long valor=0;

	while(1){

		switch(estado){

			case ESPERA:
				if((HAL_GPIO_ReadPin(Boton_GPIO_Port, Boton_Pin)) == 0){
					estado = PULSADO;
				}else{
					estado = ESPERA;
					vTaskDelay(100); // Si este no esta las tareas led no se ejecutan hasta que aprete el pulsador
				}
			break;

			case PULSADO:
				if(antirebote()){	//Fue un pulso
					estado = CAMBIO;
				}else{
					estado = ESPERA;
				}
			break;

			case CAMBIO:

				switch(aux){ //Segundo switch

					case 0: //SUBIR PRIOR LED1

						vTaskPrioritySet(pTareaLED1, 2);
						vTaskPrioritySet(pTareaLED2, 1);
						vTaskPrioritySet(pTareaLED3, 1);
						aux++;

					break;
					case 1: //SUBIR PRIOR LED2

						vTaskPrioritySet(pTareaLED1, 1);
						vTaskPrioritySet(pTareaLED2, 2);
						vTaskPrioritySet(pTareaLED3, 1);
						aux++;

					break;
					case 2: //SUBIR PRIOR LED3

						vTaskPrioritySet(pTareaLED1, 1);
						vTaskPrioritySet(pTareaLED2, 1);
						vTaskPrioritySet(pTareaLED3, 2);
						aux++;

					break;
					case 3: //Random

						do {
							valor = rand();
						} while( valor > 2 ); //Corre la funcion random hasta que me devuelva 0, 1 o 2.

						if (valor == 0){
							vTaskPrioritySet(pTareaLED1, 2);
							vTaskPrioritySet(pTareaLED2, 1);
							vTaskPrioritySet(pTareaLED3, 1);
						}else if(valor==1){
							vTaskPrioritySet(pTareaLED1, 1);
							vTaskPrioritySet(pTareaLED2, 2);
							vTaskPrioritySet(pTareaLED3, 1);
						}else{
							vTaskPrioritySet(pTareaLED1, 1);
							vTaskPrioritySet(pTareaLED2, 1);
							vTaskPrioritySet(pTareaLED3, 2);
						}
						aux++;
					break;
					default:
						vTaskPrioritySet(pTareaLED1, 1);
						vTaskPrioritySet(pTareaLED2, 1);
						vTaskPrioritySet(pTareaLED3, 1);
						aux = 0;
					break;
				}
			estado = ESPERA;
			break;
		}
	}

}

uint8_t antirebote(void){
	uint8_t retorno=0;
	vTaskDelay(100);
	if(HAL_GPIO_ReadPin(Boton_GPIO_Port, Boton_Pin)){
		retorno = 1;
	}
	return retorno;
}

unsigned long rand(void){
	static unsigned long x=0x5555AAAA,y=0x5555AAAA,z=0x5555AAAA,w=1590327;
	unsigned long t;
	t=(x^(x<<11));
	x=y;
	y=z;
	z=w;
	unsigned long r = (w=(w^(w>>19))^(t^(t>>8)));

	unsigned long pepe = (r & 3);
	return pepe; //Para debuggear
}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END 5 */ 
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
